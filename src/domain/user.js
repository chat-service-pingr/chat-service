const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema({
  _id: String,
  chatsId: [{ type: String, ref: 'Chat' }],
  createdAt: Date,
});

const User = mongoose.model('User', userSchema);

module.exports = { User };
