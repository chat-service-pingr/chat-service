const mongoose = require('mongoose');
const { Schema } = mongoose;

const chatSchema = new Schema({
  _id: String,
  usersId: [{ type: String, ref: 'User' }],
  createdAt: Date,
  messages: [{ _id: String, senderId: String, message: String, createdAt: Date, updatedAt: Date, deleted: Boolean }],
});

const Chat = mongoose.model('Chat', chatSchema);

module.exports = { Chat };
