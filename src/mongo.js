const mongoose = require('mongoose');

const connectionURL = process.env.MONGO_URL;

module.exports = mongoose.connect(connectionURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
