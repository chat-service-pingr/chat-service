const stan = require('node-nats-streaming');
const { CHAT_CREATED, CHAT_SERVICE_TOPIC } = require('./util/constants');
const db = require('./repository/chat');

module.exports = function (mongoClient) {
  const conn = stan.connect('test-cluster', 'test', {
    url: process.env.BROKER_URL,
  });

  // const dbConnection = mongoClient.db('chat-service');

  conn.on('connect', () => {
    console.log('Connected to NATS Streaming');

    const chatServiceSub = conn.subscribe(CHAT_SERVICE_TOPIC);
    chatServiceSub.on('message', async (message) => {
      // const event = JSON.parse(message.getData());
      // switch (event.eventType) {
      //   case CHAT_CREATED:
      //     db.insertChat(event, { dbConnection });
      //     break;
      // }
    });
  });

  return conn;
};
