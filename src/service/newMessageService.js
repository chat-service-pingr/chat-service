const { MESSAGE_CREATED, CHAT_SERVICE_TOPIC } = require('../util/constants');
const { v4: uuid } = require('uuid');
const { Chat } = require('../domain/chat');
const { User } = require('../domain/user');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const createNewMessage = async (request, response, { stanConn, mongoClient }) => {
  const receiverUser = await User.findById(request.body.receiverId).exec();
  const senderUser = await User.findById(request.body.senderId).exec();

  if (receiverUser && senderUser) {
    let chat = await Chat.find({ usersId: { $size: 2, $all: [receiverUser._id, senderUser._id] } }).exec();
    if (!chat) {
      chat = new Chat({
        _id: uuid(),
        usersId: [receiverUser._id, senderUser._id],
        createdAt: new Date(),
        messages: [],
      });
    }
    const currentDate = new Date();
    const message = {
      _id: uuid(),
      senderId: senderUser._id,
      message: request.body.message,
      createdAt: currentDate,
      updatedAt: currentDate,
    };
    chat.messages.push(message);

    receiverUser.chatsId.push(chat._id);
    senderUser.chatsId.push(chat._id);

    await receiverUser.save();
    await senderUser.save();
    await chat.save();

    const event = {
      eventType: MESSAGE_CREATED,
      entityAggregate: message,
    };

    stanConn.publish(senderUser._id, JSON.stringify(event));
    response.status(201).send({ messageId: message._id });
  }

  response.status(400);
};

module.exports = {
  createNewMessage,
};
