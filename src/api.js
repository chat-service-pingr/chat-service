const jwt = require('jsonwebtoken');
const express = require('express');
const cors = require('cors');
const { v4: uuid } = require('uuid');
const { createNewMessage } = require('./service/newMessageService');

module.exports = function (corsOptions, context) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get('/', (req, res) => res.json('Hello, World!'));

  api.post('/message', async (request, response) => createNewMessage(request, response, context));

  return api;
};

