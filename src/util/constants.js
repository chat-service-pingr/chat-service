const CHAT_SERVICE_TOPIC = 'chat-service';
const MESSAGE_CREATED = 'ChatCreated';

module.exports = {
  CHAT_SERVICE_TOPIC,
  MESSAGE_CREATED
}